class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :blog

  validates :content, presence: true, length: { minimum: 20, maximum: 1000 }

  # perform_later - Hey rails, whenever you get a chance haha.
  after_create_commit { CommentBroadcastJob.perform_later(self) }
end
