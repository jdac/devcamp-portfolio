class Portfolio < ApplicationRecord
  has_many :technologies
  accepts_nested_attributes_for :technologies,
                                allow_destroy: true,
                                reject_if: lambda { |attrs| attrs['name'].blank? }

  validates_presence_of :title, :body

  mount_uploader :thumb_image, PortfolioUploader
  mount_uploader :main_image, PortfolioUploader

  # Create a custom scope using a class method - The preferred way since it looks more like ruby
  def self.coldfusion
    where(subtitle: 'ColdFusion')
  end

  # Custom scope using lambda notation - not as clear, i think.
  # scope :ruby_on_rails, -> { where(subtitle: 'Ruby on Rails') }
  def self.ruby_on_rails
    where(subtitle: 'Ruby on Rails')
  end

  def self.by_position
    order("position ASC")
  end

end
