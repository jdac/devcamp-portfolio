module PortfoliosHelper
  def image_generator(height:, width:)
    "https://place-hold.it/#{height}x#{width}"
  end

  def portfolio_img(img, type)
    if img.file
      img.to_s
    elsif type == 'thumb'
      image_generator(height: '300', width: '200')
    elsif type == 'main'
      image_generator(height: '600', width: '400')
    end
  end

end


