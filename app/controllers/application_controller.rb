class ApplicationController < ActionController::Base
  include DeviseWhitelist
  include SetVisitorSource
  include CurrentUserConcern
  include DefaultPageContent
end
