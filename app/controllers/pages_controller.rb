class PagesController < ApplicationController

  def home; end

  def about
    @skills = Skill.order('percent_utilized DESC')
  end

  def contact; end

  def credentials; end

  def tech_news
    dow = Date.new(Time.now.year, Time.now.month, Time.now.day).wday
    tag = helpers.tweet_topics[dow-1]
    @tweets = SocialTool.twitter_search tag, 6
  end

end
