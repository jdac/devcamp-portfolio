class PortfoliosController < ApplicationController
  before_action :set_portfolio_item, only: %i[show edit update destroy]
  layout 'portfolio'
  access all: [:show, :index, :coldfusion, :ruby_on_rails], user: {except: [:destroy, :new, :create, :update, :edit, :sort]}, site_admin: :all

  def index
    @portfolio_items = Portfolio.by_position
  end

  def sort
    params[:order].each do |key, value|
      Portfolio.find(value[:id]).update(position: value[:position])
    end
    head :ok #bypasses the normal rails pipeline so it doesn't look for a sort template
  end

  def coldfusion
    @cf_portfolio_items = Portfolio.coldfusion  # uses a custom scope in the model defined as a class method
  end

  def ruby_on_rails
    @ruby_portfolio_items = Portfolio.ruby_on_rails  # uses a custom scope in the model defined as a scope
  end

  def new
    @portfolio_item = Portfolio.new
  end

  def create
    @portfolio_item = Portfolio.new(portfolio_params)

    respond_to do |format|
      if @portfolio_item.save
        format.html { redirect_to portfolios_path, notice: 'New portfolio item is now live' }
      else
        format.html { render :new, status: :unprocessable_entity }
      end
    end
  end

  def edit; end

  # PATCH/PUT /portfolios/1 or /portfolios/1.json
  def update
    respond_to do |format|
      if @portfolio_item.update(portfolio_params)
        format.html { redirect_to portfolios_path, notice: 'Portfolio item updated.' }
      else
        format.html { render :edit, status: :unprocessable_entity }
      end
    end
  end

  def show
  end

  def destroy
    @portfolio_item.destroy
    respond_to do |format|
      format.html { redirect_to portfolios_url, notice: 'The record has been removed.' }
    end
  end

  private

  # Never trust parameters from the scary internet, only allow white listed parameters through/
  # This is how the routing engine controls what is allowed to be passed from the form to the
  # controller.
  def portfolio_params
    params.require(:portfolio).permit(:title,
                                      :subtitle,
                                      :main_image,
                                      :thumb_image,
                                      :body,
                                      technologies_attributes: [:id, :name, :_destroy]  # the _destroy is a cocoon attr
                                      )
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_portfolio_item
    @portfolio_item = Portfolio.find(params[:id])
  end

end
