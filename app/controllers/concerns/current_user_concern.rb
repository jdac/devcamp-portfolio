module CurrentUserConcern
  extend ActiveSupport::Concern

  included do
    before_action :current_user
  end

  # Override the current_user method from Devise
  def current_user
    super || guest_user
  end

  # This implementation will crash the petergate gem because it expects to be dealing
  # with ActiveRecord type objects, not OpenStruct objects.
  #def guest_user
  #  OpenStruct.new(name: 'Guest User',
  #                 first_name: 'Guest',
  #                 last_name: 'User',
  #                 email: 'guest@guest.com'
  #                 )
  #end

  # This one works with the petergate authorization gem.
  def guest_user
    guest = GuestUser.new
    guest.name = "Guest User"
    guest.first_name = "Guest"
    guest.last_name = "User"
    guest.email = "guest@guest.com"
    guest
  end
end
