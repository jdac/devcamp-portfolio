module DefaultPageContent
  extend ActiveSupport::Concern

  included do
    before_action :set_page_defaults
  end

  def set_page_defaults
    @page_title = 'John Arnold | Portfolio Website'
    @seo_keywords = 'John Arnold  website development portfolio'
  end
end
