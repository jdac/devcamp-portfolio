ready = ->
  new Typed('.element2',
    strings: [
      'It takes serious time to achieve serious results.',
      'Aspire to being a principled software engineering enthusiast.'
    ]
    typeSpeed: 10)
  return

$(document).ready ready
$(document).on 'turbolinks:load', ready
