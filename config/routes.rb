# DON'T FORGET - If you make a change to this file, you HAVE to restart the rails
# server UNLESS you are running in the development environment.
Rails.application.routes.draw do

  resources :topics, only: [:index, :show]

  # Our custom routes for devise
  devise_for :users, path: '', path_names: { sign_in: 'login', sign_out: 'logout', sign_up: 'register' }

  resources :portfolios, except: [:show] do
    put :sort, on: :collection
  end

  get 'coldfusion-items', to: 'portfolios#coldfusion', as: 'coldfusion_items'
  get 'ruby-on-rails-items', to: 'portfolios#ruby_on_rails', as: 'ruby_on_rails_items'
  get 'portfolio/:id', to: 'portfolios#show', as: 'portfolio_show'

  resources :blogs do
    member do
      get :toggle_status
    end
  end

  # For details on the DSL available within this file, see
  # http://guides.rubyonrails.org/routing.html

  get 'about-me', to: 'pages#about', as: 'about_me'
  get 'contact', to: 'pages#contact'
  get 'cred', to: 'pages#credentials', as: 'creds'
  get 'tech-news', to: 'pages#tech_news', as: 'tech_news'

  # Set up routes for comments websocket
  mount ActionCable.server => '/cable'

  root to: 'pages#home'
end
