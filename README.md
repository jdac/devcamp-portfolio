# devcamp-portfolio Rails Application

> This is a Ruby 2.7.5 on Rails 5.2.6 application that allows users to create their own portfolios.

### Features

- Realtime chat engine for comments
- Blog
- Portfolio
- Drag and Drop Interface

### Code Example
```ruby
def my_awesome_method
  puts "Here it is"
end
```
```javascript
alert("Hi. I'm Javascript");
```
