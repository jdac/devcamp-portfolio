module SocialTool
  def self.twitter_search(tag,limit)
    client = Twitter::REST::Client.new do |config|
      config.consumer_key        = ENV.fetch("TWITTER_API_KEY")
      config.consumer_secret     = ENV.fetch("TWITTER_API_SECRET_KEY")
      config.access_token        = ENV.fetch("TWITTER_ACCESS_TOKEN")
      config.access_token_secret = ENV.fetch("TWITTER_ACCESS_TOKEN_SECRET")
    end
    client.search(tag, {result_type: 'poplular', lang: 'en', tweet_mode: 'extended'}).take(limit).collect
  end


end
